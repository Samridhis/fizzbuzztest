using System.Collections.Generic;
using FizzBuzz;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzzTestProject
{
    [TestClass]
    public class ProgramTest
    {
        private readonly FormatNumber _formatDisplay = new FormatNumber(1,100);
        private Dictionary<int, string> _valuesToCheck = new Dictionary<int, string>();

        [TestMethod]
        public void OutSideRangeReturnsEmptyTest()
        {
            var valueToCheck = 101;
            Assert.AreEqual(string.Empty, _formatDisplay.GetDisplay(valueToCheck));
            valueToCheck = -1;
            Assert.AreEqual(string.Empty, _formatDisplay.GetDisplay(valueToCheck));
        }

        [TestMethod]
        public void MaxValueTest()
        {
            const int number = 100;
            const string valueToCheck = "Buzz";
            Assert.AreEqual(valueToCheck, _formatDisplay.GetDisplay(number));
        }

        [TestMethod]
        public void NumberReturnsNumberTest()
        {
            const string valueToCheck = "1";
            const int number = 1;
            Assert.AreEqual(valueToCheck, _formatDisplay.GetDisplay(number));
        }

        [TestMethod]
        public void MultipleThreeReturnsFizzTest()
        {
            const string valueToCheck = "Fizz";
            const int number = 3;
            Assert.AreEqual(valueToCheck, _formatDisplay.GetDisplay(number));
        }

        [TestMethod]
        public void MultipleFiveReturnsBuzzTest()
        {
            const string valueToCheck = "Buzz";
            const int number = 5;
            Assert.AreEqual(valueToCheck, _formatDisplay.GetDisplay(number));
        }

        [TestMethod]
        public void MultipleThreeFiveFizzBuzzTest()
        {
            const string valueToCheck = "FizzBuzz";
            const int number = 15;
            Assert.AreEqual(valueToCheck, _formatDisplay.GetDisplay(number));
        }
    }
}