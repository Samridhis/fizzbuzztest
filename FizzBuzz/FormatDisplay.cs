﻿namespace FizzBuzz
{
    public class FormatNumber
    {
        public  int EndIndex { get; private set; } 
        public  int StartIndex { get; private set; } 

        public FormatNumber(int startIndex, int endIndex)
        {
            StartIndex = startIndex;
            EndIndex = endIndex;
        }

        public string GetDisplay(int value)
        {
            if (value < StartIndex || value > EndIndex)
                return string.Empty;
            var fizz = value % 3 == 0;
            var buzz = value % 5 == 0;
            if ( fizz && buzz )
                return "FizzBuzz";
            if (fizz)
                return "Fizz";
            if (buzz)
                return "Buzz";
            return value.ToString();
        }
    }
}