﻿using System;
using System.Text;

namespace FizzBuzz
{
    public class Program
    {
        private static void Main(string[] args)
        {
            const string appendString = " ";
            var displayText = new StringBuilder();
            var formatDisplay = new FormatNumber(1,100);
            for (var i = formatDisplay.StartIndex; i <= formatDisplay.EndIndex; i++)
            {
                displayText.Append(formatDisplay.GetDisplay(i));
                displayText.Append(appendString);
            }

            Console.WriteLine(displayText.ToString());
        }
    }
}